package com.kata.BankOCR.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Matching {

    private final Map<Integer, List<Integer>> matchingMap = new HashMap<>();

    public Matching(){
        //0
        List<Integer> list0 = new ArrayList<>();
        list0.add(8);
        matchingMap.put(0, list0);
        //1
        List<Integer> list1 = new ArrayList<>();
        list1.add(7);
        matchingMap.put(1, list1);
        //5
        List<Integer> list5 = new ArrayList<>();
        list5.add(6);
        list5.add(9);
        matchingMap.put(5, list5);
        //6
        List<Integer> list6 = new ArrayList<>();
        list6.add(5);
        list6.add(8);
        matchingMap.put(6, list6);
        //7
        List<Integer> list7 = new ArrayList<>();
        list7.add(1);
        matchingMap.put(7, list7);
        //8
        List<Integer> list8 = new ArrayList<>();
        list8.add(0);
        list8.add(6);
        list8.add(9);
        matchingMap.put(8, list8);
        //9
        List<Integer> list9 = new ArrayList<>();
        list9.add(3);
        list9.add(5);
        list9.add(8);
        matchingMap.put(9, list9);
    }

    public Map<Integer, List<Integer>> getMatchingMap() {
        return matchingMap;
    }
}
