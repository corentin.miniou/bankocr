package com.kata.BankOCR.model;

import java.util.stream.Stream;

public enum Digit {
    ZERO(" \\|\\|_ _ \\|\\|", 0),
    SEVEN("   _   \\|\\|", 7),
    EIGHT(" \\|\\|___ \\|\\|", 8),
    NINE(" \\| ___ \\|\\|", 9),
    THREE("   ___ \\|\\|", 3),
    TWO("  \\|___ \\| ", 2),
    FOUR(" \\|  _  \\|\\|", 4),
    FIVE(" \\| ___  \\|", 5),
    SIX(" \\|\\|___  \\|", 6),
    ONE("    \\|\\|", 1);

    private String expression;
    private int value;


    private Digit(String regex, int pValue){
        expression =regex;
        value =(pValue);
    }

    public String getExpression() {
        return expression;
    }


    public int getValue() {
        return value;
    }

    public static Stream<Digit> stream() {
        return Stream.of(Digit.values());
    }


}
