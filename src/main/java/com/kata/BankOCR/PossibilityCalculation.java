package com.kata.BankOCR;

import com.kata.BankOCR.model.Digit;
import com.kata.BankOCR.model.Matching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PossibilityCalculation {

    private static final Matching matching = new Matching();

    /**
     * Calculate the matching possibilities on existing numbers
     * @param line Account
     * @return Lit of possibilities
     */
    public static List<String> calculatePossibility(String line){
        List<String> possibilityList = new ArrayList<>();
        Map<Integer,List<Integer>> map = matching.getMatchingMap();
        //Remove spaces
        line = line.replaceAll(" ", "");
        //Split numbers
        String[] array = line.split("");
        StringBuilder acc = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            //Append previous characters
            if(i>0){
                acc.append(array[i - 1]);
            }
            //Convert into integer
            Integer nb = Integer.valueOf(array[i]);
            //Check if the number is concerned
            if(map.containsKey(nb)){
                List<Integer> listOfNumbers = map.get(nb);
                //End of the line
                final String endOfTheLine = line.substring(i+1, array.length);
                //For each number of list we calculate the checksum
                List<String> li= listOfNumbers.stream().filter(e-> OCR.checksum( acc.toString() + e + endOfTheLine))
                        .map(e-> acc.toString() + e + endOfTheLine).toList();
                possibilityList.addAll(li);
            }

        }
        return possibilityList;
    }

    /**
     * Guess numbers
     * @param line Account with numbers to match
     * @return List of possibilities
     */
    public static List<String> guessIllisibleNumber(String line){
        List<String> listPossibility = new ArrayList<>();
        //Regex pipe space or underscore
        String regex = "[\\| _]";

        //Tying with 9 characters
        List<String> poss9 = getPossibilities(line, regex + "{9}", 0);

        //Remove space
        List<String> poss9Reduce = poss9.stream().map(e-> e.replace("[ ]{3}","")).toList();
        //No match found : continue on 6 length
        if(poss9Reduce.isEmpty()){
            listPossibility = getPossibilities(line, regex + "{6}", 0);
        } else  {
            //Continue on 6 length with previous match
            for (String p: poss9Reduce) {
                listPossibility.addAll(getPossibilities(p, regex + "{6}", 0));
            }
        }
        //Add previous match in case all numbers were a length of 9
        listPossibility.addAll(poss9Reduce);

        //Remove remaining space
        List<String> listNoSpace = listPossibility.stream().map(e-> e.replaceAll(" ","")).toList();

        return listNoSpace.stream().filter(OCR::checksum).toList();

    }

    /**
     * Calculate possibilities with a function in parameter
     * @param line line to parse
     * @param regex Regex to search
     * @param cpt counter to avoid infinite loop
     * @return List of possibilities
     */
    private static List<String> getPossibilities(String line, String regex, int cpt) {
        List<String> possibilities = new ArrayList<>();

        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(line);

        if(m.find() && cpt<=9){
            String illegal = m.group();
            List<String> listOfPossibilities = possibility(illegal);
            if(!listOfPossibilities.isEmpty()){
                List<String> tempList = listOfPossibilities.stream().map(e -> line.replaceFirst(regex, e)).toList();
                possibilities.addAll(tempList);
                List<String> tempList2 =  new ArrayList<>();
                for (String workingLine : possibilities) {
                    tempList2.addAll(getPossibilities(workingLine, regex, cpt+1));
                }
                possibilities.addAll(tempList2);
            }
        }
        return possibilities;
    }

    /**
     * Take into parameter an illegal number and try to find a legal one
     */
    private static List<String> possibility(String errorNumber){
        List<String> probableNumbers = new ArrayList<>();
        String[] array = errorNumber.split("");
        StringBuilder acc = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if(i>0){
                acc.append(array[i - 1]);
            }
            final String endOfTheLine = errorNumber.substring(i+1, array.length);
            switch (array[i]) {
                //Add pipe or underscore
                case " " -> {
                    if (isMatching(acc + "|" + endOfTheLine) != -1) {
                        probableNumbers.add(String.valueOf(isMatching(acc + "|" + endOfTheLine)));
                    }
                    if (isMatching(acc + "_" + endOfTheLine) != -1) {
                        probableNumbers.add(String.valueOf(isMatching(acc + "_" + endOfTheLine)));
                    }
                }
                //Remove pipe or underscore
                case "_", "|" -> {
                    if (isMatching(acc + " " + endOfTheLine) != -1) {
                        probableNumbers.add(String.valueOf(isMatching(acc + " " + endOfTheLine)));
                    }
                }
            }
        }
        return probableNumbers;
    }

    private static int isMatching(String newNumber){
        //Replace regex by int value
        for (Digit d: Digit.values()) {
            if(newNumber.matches(d.getExpression())){
                return d.getValue();
            }
        }
        return -1;
    }

}
