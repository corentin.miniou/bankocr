package com.kata.BankOCR;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public final class FileUtil {

    public static File getFile(String fieName){
        //ClassLoader classLoader = getClass().getClassLoader();
        ClassLoader classLoader = FileUtil.class.getClassLoader();
        return new File(classLoader.getResource(fieName).getFile());

    }

    public static String readFile(File file) throws IOException {
        Path fileName = file.toPath();

        return Files.readString(fileName);
    }

    public static void writeFile(File file, String content) throws IOException {
        Path fileName = file.toPath();
        Files.writeString(fileName, content, StandardOpenOption.APPEND);
    }

}