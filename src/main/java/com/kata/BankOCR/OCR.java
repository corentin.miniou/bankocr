package com.kata.BankOCR;

import com.kata.BankOCR.model.Digit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class OCR {

    private static final String ERR = "ERR";
    private static final String ILL = "ILL";
    private static final String TAB = "\t";

    /**
     * Convert an entry (4 lines) into an account number
     * @param entry String to convert
     * @return Account number (9 digits)
     */
    public static String process(String entry){
        //Splitting lines
        String[] digits = entry.split("\\n");

        //Convert line into array
        char[] decomposeLine1 = digits[0].toCharArray();
        char[] decomposeLine2 = digits[1].toCharArray();
        char[] decomposeLine3 = digits[2].toCharArray();

        StringBuilder line = new StringBuilder();
        //For each cha, we concatenate characters
        for (int j = 0; j < 27; j++) {
            //Concatenate column
            line.append(decomposeLine1[j]).append(decomposeLine2[j]).append(decomposeLine3[j]);
        }

        String account= line.toString();
        //Replace regex by int value
        for (Digit d: Digit.values()) {
            account = account.replaceAll(d.getExpression(),d.getValue()+"");
        }
        account = fillResult(account);
        account = account.replaceAll(" ", "");

        return account;
    }

    /**
     * Parse file
     * @throws IOException Error on reading file
     */
    public static void convertFile(String fileName) throws IOException {
        String content = FileUtil.readFile(FileUtil.getFile(fileName));
        String[] lines= content.split("\n");

        //Read entries of the file
        List<String> entries= new ArrayList<>();
        for (int i = 0; i < lines.length; i=i+4) {
            String entry = lines[i]+ "\n";
            entry += lines[i+1]+"\n";
            entry += lines[i+2]+"\n";
            entry += lines[i+3]+"\n";
            entries.add(entry);
        }

        //For each entry convert into digits
        List<String> accounts =
                entries.stream().map(OCR::process).collect(Collectors.toList());

        //Build string to write in file
        String accountString  = String.join("\n", accounts);
        FileUtil.writeFile(FileUtil.getFile("resultat.txt"), accountString+"\n");
    }

    /**
     * Calculate the checksum of an account
     * @param entry The account number to calculate
     * @return True if the checksum is 0
     */
    public static boolean checksum(String entry){
        //Reverse string
        StringBuilder sb = new StringBuilder(entry);
        String entryReversed = sb.reverse().toString();
        //Retrieve each digit
        char[] array =  entryReversed.toCharArray();
        int sum = 0;
        //Calculate checksum
        for (int i = 0; i < array.length; i++) {
            sum += (i+1)*Character.getNumericValue(array[i]);
        }

        int checksum = sum % 11;
        return checksum == 0;
    }

    /**
     *Fill the last column with the result, nothing if checksum is ok
     * @param account Account number
     * @return Line with result
     */
    private static String fillResult(String account){
        String line = account;
        if(account.contains("|") || account.contains("_")){
            //Space, pipe and underscore
            String regex = "[\\| _]";
            List<String> possibilities = PossibilityCalculation.guessIllisibleNumber(line);
            line = lineToString(line, possibilities);
        } else if(!checksum(account)){
            List<String>  listPossibility = PossibilityCalculation.calculatePossibility(account);
            line = lineToString(line ,listPossibility);
        }

        return line;
    }

    /**
     * Print the output
     * @param possibilities List of possibilities to print
     * @return line with the right format
     */
    private static String lineToString(String account, List<String> possibilities){
        String line = account;
        //Space, pipe and underscore
        String regex = "[\\| _]";
        if(possibilities.size() == 1){
            line = possibilities.get(0);
        } else if(!possibilities.isEmpty()){
            String possibilityString=  possibilities.stream().map(e -> "'"+e+"'").collect(Collectors.joining(","));
            line = line + TAB + "AMB" + TAB + "["+possibilityString+"]";

        } else{
            line = line.replaceAll(regex + "{9}","?");
            line = line.replaceAll(regex + "{6}","?");
            line = line + TAB + ILL;
        }
      // String possibilityString=  possibilities.stream().map(e -> "'"+e+"'").collect(Collectors.joining(","));
        return line;
    }

}
