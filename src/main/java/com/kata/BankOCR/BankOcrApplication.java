package com.kata.BankOCR;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankOcrApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BankOcrApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//Retrieve fileName
		String l=args[0];
		OCR.convertFile(l);
	}
}
