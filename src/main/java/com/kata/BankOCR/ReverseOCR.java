package com.kata.BankOCR;

import com.kata.BankOCR.model.Digit;

import java.util.Random;

public class ReverseOCR {

    public static String process(){
        Random random = new Random();

        //Generate account number
        String accountNumber="";
        for (var i = 0; i < 9; i++) {
            accountNumber+=random.nextInt(10);
        }

        String account= accountNumber;
        //account = "123456789";
        //Replace regex by int value
        for (Digit d: Digit.values()) {
            account = account.replaceAll(d.getValue()+"",d.getExpression());
        }

        String line1 = "";
        String line2 = "";
        String line3 = "";
        for (int i = 0; i <account.length()-2; i=i+3) {
            line1+=  account.substring(i,i+1);
            line2+=  account.substring(i+1, i+2);
            line3+=  account.substring(i+2, i+3);

        }

        line1= fillLine(line1);
        line2= fillLine(line2);
        line3= fillLine(line3);
        String line4= fillLine("");


        return line1 + line2 + line3 + line4;
    }

    public static String fillLine(String line){
        for (int i = line.length(); i < 27 ; i++) {
            line += " ";
        }

        return line + "\n";
    }

    public static void main(String[] args) {
        process();
    }
}
